FROM pytorch/torchserve:latest


COPY ./requirements.txt /home/model-server

RUN pip install -r requirements.txt

COPY ./model.mar /home/model-server/model-store
COPY ./model_best_test.pt  /home/model-server/model-store


#CMD torchserve --start --model-store /models --models all
