import os
import sys

root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
print(root_dir)
if root_dir not in sys.path:
    sys.path.append(root_dir)

import numpy as np


from base_env import Base2DEnv


class BPP2DLayerEnv(Base2DEnv):
    def __init__(self, args):
        super(BPP2DLayerEnv, self).__init__(args)

    def reset(self):
        self.put_boxes.clear()
        self._step = 0
        self.done = False
        self.current_box_id = self._step
        self.current_box_prop = self.ids_to_box_prop[self.current_box_id]
        self.current_box_grid = self.ids_to_box_grid[self.current_box_id]
        self.curr_layer = 0
        self.height_map = np.zeros((self.grid_size, self.grid_size), dtype=np.int32)

        # observation = (height_map, mask)
        height_map = self.height_map.copy()
        mask = self.get_mask(height_map, self.current_box_grid)
        mask = np.expand_dims(mask, axis=0)

        # height_map shape: (1, grid_size, grid_size)
        # obs_mask shape: (2, grid_size, grid_size)
        # state shape: (3, grid_size, grid_size)
        state = np.expand_dims(height_map, axis=0)
        # state_mask = mask.copy().reshape(self.orientation, self.grid_size, -1)

        # obs = np.concatenate((height_map, state_mask))
        return state, mask

    def step(self, action):
        """
            - - - - - - - - - (length)
           |
           |
           |
           |
        (width)
        """
        # 1. drop the box
        # 2. get next state
        # 3. get masking
        # 3. calculate reward
        # location = [x, y, o]

        action_idx = action[0]
        location = self.actions[action_idx]
        orientation = location[2]
        current_box_id = self._step
        current_box = self.ids_to_box_prop[self._step]
        current_grid_box = self.ids_to_box_grid[self._step]

        if orientation == 0 or orientation == 2:
            box_grid_width = current_grid_box[0]  # grid size
            box_grid_length = current_grid_box[1]
            box_grid_height = current_grid_box[2]

        elif orientation == 1 or orientation == 3:
            box_grid_length = current_grid_box[0]  # grid size
            box_grid_width = current_grid_box[1]
            box_grid_height = current_grid_box[2]

        r00 = location[0]
        r01 = location[0] + box_grid_width
        r10 = location[1]
        r11 = location[1] + box_grid_length

        max_h = np.max(self.height_map[r00:r01, r10:r11])
        max_h += box_grid_height
        self.height_map[r00:r01, r10:r11] = max_h
        self._step += 1

        self.put_boxes.append([current_box, max_h])

        next_state = self.height_map.copy()
        reward = self.get_reward(location, current_box)

        dist2barloc = self.barcode_loc_to_center_loc(
            action, current_box_id, self.barcode_loc
        )

        reward += 0.5 * dist2barloc

        if int(self._step) >= len(self.box_ids):  # if all drop, done is True
            next_box = None
            self.done = True
        else:
            next_box = self.ids_to_box_grid[self._step]  # else, done is False

        mask = self.get_mask(next_state, next_box)

        # 1. layer masking
        # if layer is not full(=not done) and mask is 0, update self.curr_layer
        if self.curr_layer < int(self.floor - 1) and np.sum(mask) == 0:
            self.curr_layer += 1
            mask = self.get_mask(next_state, next_box)

        if self.curr_layer >= int(self.floor - 1) and np.sum(mask) == 0:
            self.done = True

        next_state = np.expand_dims(next_state, axis=0)
        mask = np.expand_dims(mask, axis=0)  # simple state(only height_map)

        action_to_unity = self.action_to_unity(action, current_box_id)

        return (
            next_state,
            mask.copy(),
            reward,
            self.done,
            action_to_unity,
        )

    def get_mask(self, current_state, next_box=None):
        if next_box == None:
            # is done
            action_mask1 = np.zeros((self.grid_size, self.grid_size), dtype=np.int32)
            action_mask2 = np.zeros((self.grid_size, self.grid_size), dtype=np.int32)
            action_mask3 = np.zeros((self.grid_size, self.grid_size), dtype=np.int32)
            action_mask4 = np.zeros((self.grid_size, self.grid_size), dtype=np.int32)

            mask = np.hstack(
                (
                    action_mask1.reshape(
                        -1,
                    ),
                    action_mask2.reshape(
                        -1,
                    ),
                    action_mask3.reshape(
                        -1,
                    ),
                    action_mask4.reshape(
                        -1,
                    ),
                )
            )
            return mask
        else:
            action_mask1 = np.zeros((self.grid_size, self.grid_size), dtype=np.int32)
            action_mask2 = np.zeros((self.grid_size, self.grid_size), dtype=np.int32)
            action_mask3 = np.zeros((self.grid_size, self.grid_size), dtype=np.int32)
            action_mask4 = np.zeros((self.grid_size, self.grid_size), dtype=np.int32)
            # not done, calculate masking position
            # current_state is height_map
            box_width_grid = next_box[0]
            box_length_grid = next_box[1]
            box_height_grid = next_box[2]

            for i in range(self.grid_size - box_width_grid + 1):
                for j in range(self.grid_size - box_length_grid + 1):
                    if (
                        self.check_box(
                            current_state,
                            box_width_grid,
                            box_length_grid,
                            i,
                            j,
                            box_height_grid,
                        )
                        >= 0
                    ):
                        action_mask1[i][j] = 1
                        action_mask3[i][j] = 1

            for i in range(self.grid_size - box_length_grid + 1):
                for j in range(self.grid_size - box_width_grid + 1):
                    if (
                        self.check_box(
                            current_state,
                            box_length_grid,
                            box_width_grid,
                            i,
                            j,
                            box_height_grid,
                        )
                        >= 0
                    ):
                        action_mask2[i][j] = 1
                        action_mask4[i][j] = 1

            mask = np.hstack(
                (
                    action_mask1.reshape(
                        -1,
                    ),
                    action_mask2.reshape(
                        -1,
                    ),
                    action_mask3.reshape(
                        -1,
                    ),
                    action_mask4.reshape(
                        -1,
                    ),
                )
            )
            return mask

    def check_box(self, state, box_width_grid, box_length_grid, i, j, box_height_grid):
        """
        box_width_grid: grid size of box width
        box_length: grid size of box length
        i: location width
        j: location length
        box_height: grid size of box height

        """
        # 0. check width, length boundary
        if i + box_width_grid > self.grid_size or j + box_length_grid > self.grid_size:
            return -1
        if i < 0 or j < 0:
            return -1

        # 0-1. check layer
        # if state[i, j] >

        # 1. if drop the next box, get max height value & position
        rectangle = state[i : i + box_width_grid, j : j + box_length_grid]
        r00 = rectangle[0, 0]
        r01 = rectangle[box_width_grid - 1, 0]
        r10 = rectangle[0, box_length_grid - 1]
        r11 = rectangle[box_width_grid - 1, box_length_grid - 1]
        max_pos_h = max(r00, r01, r10, r11)
        max_h = np.max(rectangle)
        # 2. whether candidate position is current layer (layer masking)
        # 현재 쌓고 있는 layer가 선택한 영역안의 최대 높이와 다르면, 놓을 수 없음(layer by layer 때문)
        if self.curr_layer != max_h:
            return -1

        # 3. check height boundary
        assert max_h >= 0
        if max_h + box_height_grid > self.floor:
            return -1

        # 4. layer masking
        # todo: layer masking and cnn -> fc, double dqn, a2c, feasible mask predict

        return max_h

    def barcode_loc_to_center_loc(self, action, box_id, barcode):
        location = self.actions[action[0]]
        orientation = location[2]

        if orientation == 0 or orientation == 2:
            box_width = self.ids_to_box_grid[box_id][0]
            box_length = self.ids_to_box_grid[box_id][1]

        elif orientation == 1 or orientation == 3:
            box_width = self.ids_to_box_grid[box_id][1]
            box_length = self.ids_to_box_grid[box_id][0]

        upto_grid_width = location[0] + box_width - 1
        upto_grid_length = location[1] + box_length - 1
        grid_width_mid = self.grid_size / 2
        grid_length_mid = self.grid_size / 2

        if barcode == 1:
            if orientation == 0:
                barcode_loc = upto_grid_width
            elif orientation == 1:
                barcode_loc = upto_grid_length
            elif orientation == 2:
                barcode_loc = location[0]
            elif orientation == 3:
                barcode_loc = location[1]
            dist = abs(barcode_loc - grid_length_mid)

        elif barcode == 2:
            if orientation == 0:
                barcode_loc = location[1]
            elif orientation == 1:
                barcode_loc = upto_grid_width
            elif orientation == 2:
                barcode_loc = upto_grid_length
            elif orientation == 3:
                barcode_loc = location[0]
            dist = abs(barcode_loc - grid_width_mid)

        elif barcode == 3:
            if orientation == 0:
                barcode_loc = location[0]
            elif orientation == 1:
                barcode_loc = location[1]
            elif orientation == 2:
                barcode_loc = upto_grid_width
            elif orientation == 3:
                barcode_loc = upto_grid_length
            dist = abs(barcode_loc - grid_length_mid)

        elif barcode == 4:
            if orientation == 0:
                barcode_loc = upto_grid_length
            elif orientation == 1:
                barcode_loc = location[0]
            elif orientation == 2:
                barcode_loc = location[1]
            elif orientation == 3:
                barcode_loc = upto_grid_width
            dist = abs(barcode_loc - grid_width_mid)

        return dist
