import torch
import scipy.signal
import json
from torch.nn.modules import loss
from operator import itemgetter


def lcm(a, b):
    for i in range(max(a, b), (a * b) + 1):
        if i % a == 0 and i % b == 0:
            return i


def draw_rect(ctx, x1, y1, x2, y2):
    ctx.move_to(x1, y1)
    ctx.line_to(x1, y2)
    ctx.line_to(x2, y2)
    ctx.line_to(x2, y1)
    ctx.close_path()
    ctx.stroke()


def get_bool(str_="True"):
    if str_ is None or str_ == "True":
        return True
    elif str_ == "False":
        return False
    else:
        raise NotImplementedError


def get_activation_fn(activation_name="selu"):
    if activation_name is None:
        return None
    elif activation_name == "silu":
        return torch.nn.SiLU
    elif activation_name == "selu":
        return torch.nn.SELU
    elif activation_name == "softmax":
        return torch.nn.Softmax
    elif activation_name == "tanh":
        return torch.nn.Tanh
    elif activation_name == "elu":
        return torch.nn.ELU
    elif activation_name == "relu":
        return torch.nn.ReLU
    elif activation_name == "sigmoid":
        return torch.nn.Sigmoid
    elif activation_name == "linear":
        return torch.nn.Linear
    elif activation_name == "softplus":
        return torch.nn.Softplus
    elif activation_name == "softsign":
        return torch.nn.Softsign
    elif activation_name == "leaky_relu":
        return torch.nn.LeakyReLU
    else:
        return torch.nn.Tanh


def get_optimizer(optimizer_name="adam"):
    if optimizer_name is None:
        return None
    elif optimizer_name == "adam":
        return torch.optim.Adam
    elif optimizer_name == "rmsprop":
        return torch.optim.RMSprop
    elif optimizer_name == "adadelta":
        return torch.optim.Adadelta
    elif optimizer_name == "adagrad":
        return torch.optim.Adagrad
    elif optimizer_name == "sgd":
        return torch.optim.SGD
    elif optimizer_name == "asgd":
        return torch.optim.ASGD
    elif optimizer_name == "lbfgs":
        return torch.optim.LBFGS
    else:
        return torch.optim.Adam


def get_loss_func(loss_func_name="smoothl1"):
    if loss_func_name is None:
        return None
    if loss_func_name == "smoothl1":
        return loss.SmoothL1Loss()
    elif loss_func_name == "mae":
        return loss.L1Loss()
    elif loss_func_name == "mse":
        return loss.MSELoss()
    elif loss_func_name == "cosine":
        return loss.CosineEmbeddingLoss()
    elif loss_func_name == "hinge":
        return loss.HingeEmbeddingLoss()
    else:
        return loss.MSELoss()


def conv2d_output_shape(h, w, kernel_size=1, stride=1, padding=0, dilation=1):
    """
    Returns output H, W after convolution/pooling on input H, W.
    """
    kh, kw = kernel_size if isinstance(kernel_size, tuple) else (kernel_size,) * 2
    sh, sw = stride if isinstance(stride, tuple) else (stride,) * 2
    ph, pw = padding if isinstance(padding, tuple) else (padding,) * 2
    d = dilation
    h = (h + (2 * ph) - (d * (kh - 1)) - 1) // sh + 1
    w = (w + (2 * pw) - (d * (kw - 1)) - 1) // sw + 1
    return h, w


def discount_cumsum(x, discount):
    return scipy.signal.lfilter([1], [1, float(-discount)], x[::-1], axis=0)[::-1]


def staggered_stacking():
    pass


def to_json(coords_list, file_name):
    with open(file_name, "w") as f:
        json.dump(coords_list, f)


def reorder(action_list, total_floor):
    actions_list_sort_y = sorted(action_list, key=itemgetter(2))

    same_floor = {}
    floor = actions_list_sort_y[0][2]
    init_floor = floor
    idx = 1
    # 이건 임시
    for i in range(len(actions_list_sort_y)):

        y_val = actions_list_sort_y[i][2]
        if floor - y_val != 0:
            box_height = abs(floor - y_val)
            break

    # 어차피 박스 높이 입력값으로 들어오니 그걸 넣으면 될듯
    # box_height =

    for i in range(len(actions_list_sort_y)):
        y_val = actions_list_sort_y[i][2]

        if floor == y_val:
            if str(idx) in same_floor:
                box_coords = actions_list_sort_y[i]
                box_coords[2] = init_floor + (idx - 1) * box_height
                same_floor[str(idx)].append(actions_list_sort_y[i])
            else:
                same_floor[str(idx)] = [actions_list_sort_y[i]]

        else:
            floor = y_val
            idx += 1
            if str(idx) in same_floor:
                same_floor[str(idx)].append(actions_list_sort_y[i])
            else:
                same_floor[str(idx)] = [actions_list_sort_y[i]]

    same_floor_sorted = {}

    for i in range(len(same_floor)):
        same_floor_sorted[str(i + 1)] = sorted(
            same_floor[str(i + 1)], key=itemgetter(1)
        )

    floor_list = {}
    for i in range(1, total_floor + 1):
        if i % 2 == 0:
            even_floor_box = same_floor_sorted["2"]

            for j in range(len(even_floor_box)):
                box_coords = even_floor_box[j]
                new_box_pos = init_floor + (i - 1) * box_height
                new_box_coords = [
                    box_coords[0],
                    box_coords[1],
                    new_box_pos,
                    box_coords[3],
                    box_coords[4],
                    box_coords[5],
                    box_coords[6],
                ]
                if str(i) in floor_list:
                    floor_list[str(i)].append(new_box_coords)
                else:
                    floor_list[str(i)] = [new_box_coords]
        if i % 2 == 1:
            odd_floor_box = same_floor_sorted["1"]
            for j in range(len(odd_floor_box)):
                box_coords = odd_floor_box[j]
                new_box_pos = init_floor + (i - 1) * box_height
                new_box_coords = [
                    box_coords[0],
                    box_coords[1],
                    new_box_pos,
                    box_coords[3],
                    box_coords[4],
                    box_coords[5],
                    box_coords[6],
                ]
                if str(i) in floor_list:
                    floor_list[str(i)].append(new_box_coords)
                else:
                    floor_list[str(i)] = [new_box_coords]

    new_action_list = []
    box_number = 0
    for key in floor_list:
        for i in range(len(floor_list[key])):
            box_coord = floor_list[key][i][1:]
            box_coord.insert(0, box_number)
            new_action_list.append(box_coord)
            box_number += 1

    return new_action_list


def json_format(action_list):
    keys = ["box_number", "x", "y", "z", "rotx", "roty", "rotz"]
    for_json = dict()
    for_json["result"] = []
    for i in range(len(action_list)):
        for_json["result"].append(dict(zip(keys, action_list[i])))
    return for_json

def makeInputForm(param):
    jsonData = {}
    jsonData["pallet_width"] = float(param.pallet[0])
    jsonData['pallet_length'] = float(param.pallet[1])
    jsonData['box_ids'] = [i for i in range(0, int(param.boxNumber))]
    jsonData['floor'] = int(param.layer)
    jsonData['barcode_loc'] = float(param.barcodeLocation)
    jsonData['box_width'] = [float(param.boxSize[0]) for i in range(0, int(param.boxNumber))]
    jsonData['box_length'] = [float(param.boxSize[1]) for i in range(0, int(param.boxNumber))]
    jsonData['box_height'] = [float(param.boxSize[2]) for i in range(0, int(param.boxNumber))]
    jsonData['origin_point'] = [-7.75, 25.0, (
            (-0.5 * float(param.pallet[1]) - 0.5 * float(param.boxSize[2])) * 2 + float(param.pallet[1])) * 0.5]
    return jsonData