#!/bin/bash
docker stop torchserve-test
docker rm -f torchserve-test
docker run --name torchserve-test  -d -p 8080:8080 -p 8081:8081 --cpus=2 -m="4G" torchserve:0.1 torchserve --start --model-store /home/model-server/model-store --models all
