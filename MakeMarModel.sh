#!/bin/bash
torch-model-archiver --model-name model \
--version 1.0 \
--serialized-file model_best_test.pt \
--extra-files base_env.py,config.py,feasible_ppo.py,Ina3denv.py,inferencing.py,utils.py \
--handler handler.py \
--export-path ./ -f
