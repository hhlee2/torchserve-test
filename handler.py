import os
import sys
import numpy as np
import torch
import torch.nn as nn
from torch.distributions import Categorical
from torch.nn.modules import loss
import torch.nn.functional as F
from tqdm import tqdm
from math import ceil
from ts.torch_handler.base_handler import BaseHandler
from operator import itemgetter
import config
from Ina3denv import BPP2DLayerEnv
from feasible_ppo import PPO
from utils import (
    get_activation_fn,
    get_loss_func,
    get_optimizer,
    reorder,
)
from inferencing import run
def makeJsonForServe(param, modelname):
    jsonData = {}
    jsonData["pallet_width"] = float(param['pallet'][0])
    jsonData['pallet_length'] = float(param['pallet'][1])
    jsonData['box_ids'] = [i for i in range(0, int(param["boxNumber"]))]
    jsonData['floor'] = int(param["layer"])
    jsonData['barcode_loc'] = float(param["barcodeLocation"])
    jsonData['box_width'] = [float(param["boxSize"][0]) for i in range(0, int(param["boxNumber"]))]
    jsonData['box_length'] = [float(param["boxSize"][1]) for i in range(0, int(param["boxNumber"]))]
    jsonData['box_height'] = [float(param["boxSize"][2]) for i in range(0, int(param["boxNumber"]))]
    jsonData['origin_point'] = [-1500.0, 4000.0,448.25]
           # (-0.5 + 0.5 * float(jsonData['box_height'][0]))]
    jsonData['model_name'] = modelname
    return jsonData


class StackerServing(BaseHandler):
    def __init__(self):
        self.run = run
        self.initialized = False
        self.data=None
        #self.model_path="/Users/hohyeon/Desktop/app/common/model_best_test.pt"
        self.model_path="/home/model-server/model-store/model_best_test.pt"
    def initialize(self,data):
        self.initialized = True
        return data
    def preprocess(self,data):
        data=data[0]['body']
        data=makeJsonForServe(data,"model_best_test.pt")
        self.data=data
        self.data=data
        print(data)
        return data
    def inference(self,data):
        return run(self.data,self.model_path)
    def postprocess(self,data):
        result = self.inference(data)
        tmp=[]
        tmp.append(result)
        return tmp

