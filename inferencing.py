import os
import sys
import torch
import json

root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
if root_dir not in sys.path:
    sys.path.append(root_dir)
import config
from Ina3denv import BPP2DLayerEnv
from feasible_ppo import PPO
from utils import (
    get_activation_fn,
    get_loss_func,
    get_optimizer,
    reorder,
)


def run(testing_config, model_path):
    """
    For inferencing
    Input: config from UI, model path
    Output: str(list of list actions)
    """
    agent_eval = True
    device = "cpu"
    testing_config.update(
        {
            "grid_size": config.grid_size,
            "orientation": config.orientation,
            "r_type": config.r_type,
        }
    )
    env = BPP2DLayerEnv(testing_config)

    conv_activation = get_activation_fn(config.conv_activation)
    fc_activation = get_activation_fn(config.fc_activation)
    value_loss_func = get_loss_func(config.value_loss_func)
    mask_loss_func = get_loss_func(config.mask_loss_func)
    optimizer_func = get_optimizer(config.optimizer_func)
    buf_size = env.episode_length
    # Init Agent
    agent = PPO(
        grid_size=(env.grid_size, env.grid_size, 1),
        action_dim=env.action_dim,
        # Model Parameter
        hidden_size=config.hidden_size,
        kernel_size=config.kernel_size,
        conv_activation=conv_activation,
        fc_activation=fc_activation,
        learning_rate=config.learning_rate,
        discount=config.discount_factor,
        shared_net=config.shared_net,
        optimizer_func=optimizer_func,
        # Agent Parameter
        buf_size=buf_size,
        train_iters=config.train_iters,
        clipping_ratio=config.clip_ratio,
        gae_lambda=config.gae_lambda,
        clip_norm=config.clip_norm,
        entropy_ratio=config.entropy_ratio,
        value_loss_func=value_loss_func,
        mask_loss_func=mask_loss_func,
        mask_predict=config.mask_predict,
        # Util Parameter
        device=device,
    ).to(device)
    if not os.path.exists(model_path):
        raise FileNotFoundError

    checkpoint_model = torch.load(model_path, map_location=device)
    agent.load_state_dict(checkpoint_model[f"model"])

    obs, masks = env.reset()
    done = False
    step = 0
    total_reward = 0
    actions = []
    for o in range(env.orientation):
        for w in range(env.grid_size):
            for l in range(env.grid_size):
                actions.append([w, l, o])
    action_list = []
    while not done:
        next_obs, masks, reward, done, action_to_unity = env.step(
            [agent.step(obs, masks, eval=agent_eval, device=device)]
        )
        action_list.append(action_to_unity)
        total_reward += reward
        obs = next_obs
        step += 1

    return reorder(env.coords_list, testing_config["floor"])
